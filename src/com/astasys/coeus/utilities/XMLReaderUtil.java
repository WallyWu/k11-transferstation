package com.astasys.coeus.utilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class XMLReaderUtil {
	public static void main(String[] args) {
		ReadPPSResponseXml("C:\\Users\\ASTA\\Desktop\\TransferStation\\response.xml");
	}
	
	public static Map<String, String> ReadPPSResponseXml(String xml) {
		Map<String, String> result = new HashMap<String, String>();
		InputStream is = null;
		try {
			File xmlfile = new File(xml);
			SAXReader reader = new SAXReader();
			//is = (new FileInputStream(xmlfile));
			is = new ByteArrayInputStream(xml.getBytes("UTF-8"));
			Document doc = reader.read(is);
			// System.out.println(doc.getStringValue());
			Element envelopeE = doc.getRootElement();
			Element bodyE;
			Element resultE;
			String KVJsonStr = "";

			bodyE = envelopeE.element("Body");
			resultE = bodyE.element("ReplyInfo").element("AddResponse");
			List<Element> els = resultE.elements();
			for(Element el : els) {
				result.put(el.getName(), el.getText());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != is) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	public static Map<String, String> ReadXml(String xml) {
		Map<String, String> result = new HashMap<String, String>();
		InputStream is = null;
		try {
			SAXReader reader = new SAXReader();
			is = new ByteArrayInputStream(xml.getBytes("UTF-8"));
			Document doc = reader.read(is);
			// System.out.println(doc.getStringValue());
			Element envelopeE = doc.getRootElement();
			Element bodyE;
			Element responseE;
			Element resultE;
			String KVJsonStr = "";

			bodyE = envelopeE.element("Body");

			responseE = bodyE.element("TokenRequestResponse");
			resultE = responseE.element("TokenRequestResult");
			System.out.println(resultE.getStringValue().trim());
			KVJsonStr = resultE.getStringValue().trim();
			Gson gson = new Gson();
			List<Map<String, String>> list = gson.fromJson(KVJsonStr, new TypeToken<List<Map<String, String>>>() {
			}.getType());
			if (list.size() == 1) {
				result = list.get(0);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != is) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return result;
	}
	public static Map<String, String> ReadXml(File xml) {
		Map<String, String> result = new HashMap<String, String>();
		InputStream is = null;
		try {
			SAXReader reader = new SAXReader();
			is = new FileInputStream(xml);
			/*
			 * public Map<String,String> ReadXml(String xml) { InputStream is = null; try {
			 * 
			 * is = new ByteArrayInputStream(xml.getBytes("UTF-8"));
			 */
			Document doc = reader.read(is);
			// System.out.println(doc.getStringValue());
			Element envelopeE = doc.getRootElement();
			Element bodyE;
			Element responseE;
			Element resultE;
			String KVJsonStr = "";

			bodyE = envelopeE.element("Body");

			responseE = bodyE.element("TokenRequestResponse");
			resultE = responseE.element("TokenRequestResult");
			System.out.println(resultE.getStringValue().trim());
			KVJsonStr = resultE.getStringValue().trim();
			Gson gson = new Gson();
			List<Map<String, String>> list = gson.fromJson(KVJsonStr, new TypeToken<List<Map<String, String>>>() {
			}.getType());
			if (list.size() == 1) {
				result = list.get(0);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != is) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

}
