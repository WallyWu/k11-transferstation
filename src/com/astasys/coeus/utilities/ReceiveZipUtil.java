package com.astasys.coeus.utilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;

import com.astasys.coeus.controller.TransferController;
import com.astasys.coeus.crypto.CryptoStreamProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;

public class ReceiveZipUtil {

	private static Logger logger = Logger.getLogger(TransferController.class);

	
	
	public static File decoderAndSave(Map<String, String> map, String destination) throws Exception {

		String fileName = map.get("FileName");
		String fileContent = map.get("FileContent");
		String pwd = map.get("Password");
		String xmlFileName = fileName.substring(0,fileName.lastIndexOf("."))+".xml";
		File xmlFile = newFile(destination, xmlFileName);

		// create a xml, for recording zip file and password
		logger.info("recording zip file and password --- Start");
		XMLWriter writer = null;
		try {
			Document document = DocumentHelper.createDocument();
			Element root = document.addElement("root");

			Element documentEl = root.addElement("document");

			documentEl.addElement("field").addAttribute("name", "File Name").addText(fileName);

			documentEl.addElement("field").addAttribute("name", "Password").addText(pwd);

			documentEl.addElement("field").addAttribute("name", "Execution Date")
					.addText(new SimpleDateFormat("yyyyMMdd HHmmss").format(new Date()));

			OutputFormat format = OutputFormat.createPrettyPrint();
			writer = new XMLWriter(new FileOutputStream(xmlFile), format);
			writer.write(document);
			logger.info("recording zip file and password --- End");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			logger.error("recording zip file and password --- Error --- UnsupportedEncodingException");
			throw new Exception("exception.recording.unsupportedEncodingException.failed");
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("recording zip file and password --- Error --- IOException");
			throw new Exception("exception.recording.failed");
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			logger.info("decoder ContentString By Base64 --- Start");
			Decoder decoder = Base64.getDecoder();
			byte[] b = decoder.decode(fileContent);
			for (int i = 0; i < b.length; ++i) {
				{// 调整异常数据
					if (b[i] < 0)
						b[i] += 256;
				}
			}

			OutputStream out = new FileOutputStream(newFile(destination, fileName));
			out.write(b);
			out.flush();
			out.close();
			logger.info("decoder ContentString By Base64 --- End");
		} catch (IOException e) {
			// TODO: handle exception
			logger.error("decoder ContentString By Base64 --- Error --- IOException");
			throw new Exception("exception.decoder.content.failed");
		}

		return xmlFile.getParentFile();
	}

	private static File newFile(String destination, String fileName) {
		String dateStr = new SimpleDateFormat("yyyyMMdd").format(new Date());

		File parent = new File(destination + "/" + dateStr);
		if (parent.exists()) {
			return new File(destination + "/" + dateStr + "/" + fileName);
		} else {
			parent.mkdirs();
			return new File(destination + "/" + dateStr + "/" + fileName);
		}
	}

	public static File outputFile(File directory, String outputTargetLocation,CryptoStreamProvider cryptoStreamProvider) throws Exception {
		
		//File csvFile = null;
		String csvFilePath = "";
		File encryptFile = null;
		boolean flag = false;
		// zipParentFolder name = yyyyMMdd ->directory
		// read xml and get password for output csv
		List<File> descriptionXMLs = new ArrayList<File>(
				FileUtils.listFiles(directory, new String[] { "xml", "XML" }, true));
		if (descriptionXMLs.size() < 1) {
			// error. There should be one xml
			logger.error("outputFile: Can not find readme.xml");
			throw new Exception("exception.output.xml.miss");
		} else if (descriptionXMLs.size() > 1) {
			// error. There should be only one xml
			logger.error("outputFile: Redundant readme.xml");
			throw new Exception("exception.output.xml.redundant");
		} else {
			File descriptionXML = descriptionXMLs.get(0);
			if (descriptionXML.exists() && !descriptionXML.isHidden()) {

				XMLWriter writer = null;
				Map<String, String> zipMap = new HashMap<String, String>();
				try {

					SAXReader reader = new SAXReader();
					Document xml = reader.read(new FileInputStream(descriptionXML));

					Element root = xml.getRootElement();
					Element documentEl = root.element("document");
					List<Element> els = documentEl.elements("field");
					for (Element e : els) {
						zipMap.put(e.attribute("name").getValue(), e.getText());
					}

					// extract csv file by password
					String pwd = zipMap.get("Password");
					String zipName = zipMap.get("File Name");
					//File extractTargetDir = null;
					String ExtractDateStr = new SimpleDateFormat("yyyyMMdd HHmmss").format(new Date());
					File zipFile = null;
					try {
						zipFile = new File(directory, zipName);
						csvFilePath = extractFileFromZip(zipFile, pwd, outputTargetLocation);
					} catch (ZipException zipE) {
						//recording
						documentEl.addElement("field").addAttribute("name", "Status").addText("Extract Failed");
						documentEl.addElement("field").addAttribute("name", "Extract Date").addText(ExtractDateStr);
						OutputFormat format = OutputFormat.createPrettyPrint();
						writer = new XMLWriter(new FileOutputStream(descriptionXML), format);
						writer.write(xml);
						flag = false;
						throw new ZipException("exception.output.zipfile.extract.failed");
					}
					//encrypt csv and delete origin csv and zip
					if(zipFile!=null) {
						zipFile.delete();
					}
					File csvFile = new File(csvFilePath);
					encryptFile = new File(csvFilePath.substring(0,csvFilePath.lastIndexOf("."))+".edf");
					FileCopyUtils.copy(new FileInputStream(csvFile),cryptoStreamProvider.writeTo(encryptFile));
					csvFile.delete();
					
					//recording
					documentEl.addElement("field").addAttribute("name", "Status").addText("Extract Successfully");
					documentEl.addElement("field").addAttribute("name", "Extract Date").addText(ExtractDateStr);
					documentEl.addElement("field").addAttribute("name", "Target Path").addText(encryptFile.getAbsolutePath());
					
					OutputFormat format = OutputFormat.createPrettyPrint();
					writer = new XMLWriter(new FileOutputStream(descriptionXML), format);
					writer.write(xml);
					flag = true;
					
					
				} catch (ZipException zipe) {
					throw zipe;
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("outputFile: Read XML Failed...");
					throw new Exception("exception.output.xml.read.failed");
				} finally {
					try {
						if (writer != null)
							writer.close();
						if (descriptionXML!=null) {
							if(flag) {
								String importedXML = descriptionXML.getAbsolutePath()+".imported";
								logger.info("xml after reading "+importedXML);
								descriptionXML.renameTo(new File(importedXML));
							} else {
								String importedXML = descriptionXML.getAbsolutePath()+".err";
								logger.info("xml after reading "+importedXML);
								descriptionXML.renameTo(new File(importedXML));
							}
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} 
		}
		return encryptFile;
	}

	
	private static String extractFileFromZip(File file, String pwd, String targetLocation) throws ZipException {
		logger.info("extractFileFromZip: Start Extract");
		
		String csvFilePath = "";
		
		String targetDir = targetLocation+"/"+file.getParentFile().getName();
		logger.info("ExtractFile Target: "+targetDir);
		//String targetDir = "C:/k11_OutputFile/"+file.getParentFile().getName();
		File targetFolder = new File(targetDir);
		if(!targetFolder.exists()) {
			targetFolder.mkdirs();
		}
		try {
			ZipFile zipFile = new ZipFile(file);
			if (zipFile.isEncrypted()) {
				zipFile.setPassword(pwd.toCharArray());
			}
			zipFile.extractAll(targetDir);
			List<FileHeader> list = zipFile.getFileHeaders();
			if(list!=null && list.size()>0) {
				csvFilePath = targetDir+"/"+list.get(0).getFileName();
			}
			
			
			
		} catch (ZipException e) {
			e.printStackTrace();
			logger.error("extractFileFromZip: Error in extract");
			throw e;
		}
		
		logger.info("extractFileFromZip: Complete Extract");
		
		return csvFilePath;
	}
	
	
	  
}
