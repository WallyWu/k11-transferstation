package com.astasys.coeus.utilities;

public enum MyDateFormat {

	NWDDATE("NWDDate", "yyyyMMddHHmmss"), PPSDATE("PPSDate", "dd/MM/yyyy");

	private String name;
	private String format;

	private MyDateFormat(String name, String format) {
		this.name = name;
		this.format = format;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
	

}
