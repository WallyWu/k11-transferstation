package com.astasys.coeus.crypto;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.SecureRandom;


public class SecretKeyFactoryBean implements FactoryBean, InitializingBean {
  private static Logger logger = Logger.getLogger(SecretKeyFactoryBean.class);
  private final byte[] seed = {
    76, 7, 80, -106, 85, -82, 99, 36, -100, -120, -84, -71, 99, 66, -89, -69
  };
  private SecretKey secretKey;
  private Resource secretKeyLocation;
  private String cryptoImplementation = CryptoStreamProvider.AES;


  public void setSecretKeyLocation(Resource secretKeyLocation) {
    this.secretKeyLocation = secretKeyLocation;
  }

  public void setCryptoImplementation(String cryptoImplementation) {
    this.cryptoImplementation = cryptoImplementation;
  }

  public Object getObject() throws Exception {
    return secretKey;
  }

  public Class getObjectType() {
    return secretKey != null ? secretKey.getClass() : SecretKey.class;
  }

  public boolean isSingleton() {
    return true;
  }

@Override
public void afterPropertiesSet() throws Exception {
	// TODO Auto-generated method stub

    final File keyFile = secretKeyLocation.getFile();
    if (keyFile.exists()) {
      logger.info("Reading secret key from [" + keyFile.getAbsolutePath() + "]");
      ObjectInputStream in = new ObjectInputStream(secretKeyLocation.getInputStream());
      secretKey = (SecretKey) in.readObject();
    } else {
      logger.info("Secret key not found, creating one");
      KeyGenerator keyGenerator = KeyGenerator.getInstance(cryptoImplementation);
      keyGenerator.init(new SecureRandom(seed));
      secretKey = keyGenerator.generateKey();
      ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(keyFile));
      out.writeObject(secretKey);
      out.flush();
      out.close();

      logger.info("!===== WARNING ===== WARNING ===== WARNING =====!");
      logger.info("A new secret key will be generated at [" + keyFile.getAbsolutePath() + "]");
      logger.info("Please backup this key NOW!");
      logger.info("If this key is damaged, corrupted or disappeared for any reason,");
      logger.info("you will not be able to recover the encrypted files in any way!");
      logger.info("!===== WARNING ===== WARNING ===== WARNING =====!");

      
    }
  
}

  
}
