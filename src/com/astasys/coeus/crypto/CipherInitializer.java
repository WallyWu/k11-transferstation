package com.astasys.coeus.crypto;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;

public interface CipherInitializer {
  CipherInitializer SECRET_KEY_BASED =
      new CipherInitializer() {

        public Cipher initializeEncryptMode(SecretKey secretKey, Cipher cipher)
            throws InvalidKeyException {
          cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec(secretKey, cipher));
          return cipher;
        }

        public Cipher initializeDecryptMode(SecretKey secretKey, Cipher cipher)
            throws InvalidKeyException {
          cipher.init(Cipher.DECRYPT_MODE, secretKeySpec(secretKey, cipher));
          return cipher;
        }

        private SecretKeySpec secretKeySpec(SecretKey secretKey, Cipher cipher) {
          return new SecretKeySpec(secretKey.getEncoded(), cipher.getAlgorithm());
        }
      };

  Cipher initializeEncryptMode(SecretKey secretKey, Cipher cipher)
      throws InvalidKeyException;

  Cipher initializeDecryptMode(SecretKey secretKey, Cipher cipher)
      throws InvalidKeyException;
}
