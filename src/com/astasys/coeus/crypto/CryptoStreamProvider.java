package com.astasys.coeus.crypto;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Base64;

public class CryptoStreamProvider implements InitializingBean {
  public static final String AES = "AES";
  private static Logger logger = Logger.getLogger(SecretKeyFactoryBean.class);
  private SecretKey secretKey;

  private String cryptoImplementation = CryptoStreamProvider.AES;

  private CipherInitializer cipherInitializer = CipherInitializer.SECRET_KEY_BASED;

  public void setCryptoImplementation(String cryptoImplementation) {
    this.cryptoImplementation = cryptoImplementation;
  }

  public void setSecretKey(SecretKey secretKey) {
    this.secretKey = secretKey;
  }

  public void setCipherInitializer(CipherInitializer cipherInitializer) {
    this.cipherInitializer = cipherInitializer;
  }

  public void afterPropertiesSet() throws Exception {
    Assert.notNull(secretKey);
  }

  public String encryptText(String s) {
    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      Cipher cipher =
          cipherInitializer.initializeEncryptMode(
              secretKey, Cipher.getInstance(cryptoImplementation));
      try (CipherOutputStream c = new CipherOutputStream(out, cipher)) {
        IOUtils.write(s, c, "UTF-8");
      }
      return Base64.getEncoder().encodeToString(out.toByteArray());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public String decryptText(String s) {
    try {
      InputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(s));
      Cipher cipher =
          cipherInitializer.initializeDecryptMode(
              secretKey, Cipher.getInstance(cryptoImplementation));
      try (CipherInputStream c = new CipherInputStream(in, cipher)) {
        return IOUtils.toString(c, "UTF-8");
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
  
  public OutputStream writeTo(File file) throws IOException {
	    try {
	      return new CipherOutputStream(
	          new BufferedOutputStream(new FileOutputStream(file)),
	          cipherInitializer.initializeEncryptMode(
	              secretKey, Cipher.getInstance(cryptoImplementation)));
	    } catch (GeneralSecurityException e) {
	      throw new IOException("Cannot obtain CipherOutputStream");
	    } catch (FileNotFoundException e) {
	      throw new IOException("Cannot obtain file input stream");
	    }
	  }

}
