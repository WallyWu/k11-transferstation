package com.astasys.coeus.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.w3c.dom.NodeList;

import com.astasys.coeus.crypto.CryptoStreamProvider;
import com.astasys.coeus.utilities.MyDateFormat;
import com.astasys.coeus.utilities.ReceiveZipUtil;
import com.astasys.coeus.utilities.XMLReaderUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;


@Controller
@RequestMapping("/transfer")
public class TransferController {

	private static Logger logger = Logger.getLogger(TransferController.class);

	private final static String[] DATE_FORMAT = { "NWDDate", "PPSDate" };

	@Autowired
	private CryptoStreamProvider cryptoStreamProvider;

	@Value("classpath:/TSRequestNWD.template")
	private Resource tsRequestNWDTemplate;

	@Value("classpath:/TSRequestPPS.template")
	private Resource tsRequestPPSTemplate;

	@Value("${server.enableIPLimit}")
	boolean enableIPLimit;

	@Value("${server.whiteIP}")
	String whiteListStr;

	@Value("${server.soap.nwd.url}")
	String nwd_url;

	@Value("${server.soap.nwd.action.url.request}")
	String nwd_SOAPAction_url_request;

	@Value("${server.soap.nwd.action.url.verify}")
	String nwd_SOAPAction_url_verify;

	@Value("${server.soap.nwd.action.request}")
	String nwd_SOAPAction_request;

	@Value("${server.soap.nwd.action.verify}")
	String nwd_SOAPAction_verify;

	@Value("${server.soap.nwd.namespace}")
	String nwd_nameSpace;

	@Value("${server.soap.nwd.namespace.url}")
	String nwd_nameSpace_url;

	@Value("${server.soap.nwd.request.attributes.element}")
	String nwd_request_elements;

	@Value("${server.soap.pps.url}")
	String pps_url;

	@Value("${server.soap.pps.action.url}")
	String pps_SOAPAction_url;

	@Value("${server.soap.pps.action}")
	String pps_SOAPAction;

	@Value("${server.soap.pps.namespace}")
	String pps_nameSpace;

	@Value("${server.soap.pps.namespace.url}")
	String pps_nameSpace_url;

	@Value("${server.soap.pps.request.attributes.element}")
	String pps_request_elements;
	
	@Value("${server.soap.pps.uat.url}")
	String pps_uat_url;
	
	@Value("${server.soap.pps.uat.action.url}")
	String pps_uat_SOAPAction_url;

	@Value("${server.soap.pps.uat.action}")
	String pps_uat_SOAPAction;

	@Value("${server.soap.pps.uat.namespace}")
	String pps_uat_nameSpace;

	@Value("${server.soap.pps.uat.namespace.url}")
	String pps_uat_nameSpace_url;

	@Value("${server.soap.pps.uat.request.attributes.element}")
	String pps_uat_request_elements;

	@Value("${zipfile.save.destination}")
	String destination;
	
	@Value("${zipfile.backup.destination}")
	String bak_destination;
	
	@Value("${masterlist.output.location}")
	private String outputTargetLocation;
	
	@Value("${token.timeLimit.min}")
	private int timeLimitMin;

	public void setTsRequestNWDTemplate(Resource tsRequestNWDTemplate) {
		this.tsRequestNWDTemplate = tsRequestNWDTemplate;
	}

	public void setTsRequestPPSTemplate(Resource tsRequestPPSTemplate) {
		this.tsRequestPPSTemplate = tsRequestPPSTemplate;
	}

	ThreadLocal<String> NWDToken = new ThreadLocal<String>();
	ThreadLocal<String> OTPwd = new ThreadLocal<String>();

	Long timeLimit = timeLimitMin * 60 * 1000L;

	@RequestMapping("/triggerImportForData")
	public void triggerImportForData(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String ekey = request.getParameter("Key");
		String dkey = cryptoStreamProvider.decryptText(ekey);

		String encryptCSVFilePath = dkey.substring(0, dkey.indexOf(" "));
		Long keyDateLong = Long.valueOf(dkey.substring(dkey.indexOf(" ") + 1, dkey.length()));
		Date nowDate = new Date();
		if ((nowDate.getTime() - timeLimit) > keyDateLong) {
			logger.debug("Over limit time "+ timeLimitMin +" mins. Expired Token");
			writeJsonStringToResponse(response, false, "Over limit time "+ timeLimitMin +" mins. Expired Token");
		} else {
			logger.debug("Importing User...");
			// do import
			File csvFile = new File(encryptCSVFilePath);
			writeFileToResponse(response, true, csvFile);

		}

	}

	/**
	 * Description: triggerRequestForData and triggerRequestForDataByXML are two
	 * feature but same result triggerRequestForData: send soap with soap develop
	 * jar triggerRequestForDataByXML: send soap with xml
	 * @throws DocumentException 
	 */

	
	public void triggerRequestForData(HttpServletRequest request, HttpServletResponse response) throws IOException, DocumentException {
		logger.info("====== Start triggerRequestForData");
		String ip = request.getRemoteAddr();
		if (enableIPLimit) {
			List<String> whiteIPList = new ArrayList<String>(Arrays.asList(whiteListStr.split(",")));
			if (!whiteIPList.contains(ip)) {
				logger.warn("====== IP not in white List. Reject!!!");
				writeJsonStringToResponse(response, false, "IP not in white List. Reject!!!");
				return;
			}
		}

		try {
			//
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			
			SOAPMessage nwdSOAPTokenRequest = createSOAPRequest(nwd_SOAPAction_url_request, nwd_nameSpace_url,
					nwd_nameSpace, nwd_request_elements, nwd_SOAPAction_request);
			SOAPMessage nwdSOAPTokenResponse = soapConnection.call(nwdSOAPTokenRequest, nwd_url);
			// Print the SOAP Response
			/*
			 * System.out.println("Response SOAP Message:");
			 * nwdSOAPTokenResponse.writeTo(System.out); System.out.println();
			 */

			SOAPBody nwdSOAPTokenResponseBody = nwdSOAPTokenResponse.getSOAPBody();
			NodeList tokenRequestResponseNodes = nwdSOAPTokenResponseBody.getChildNodes(); // TokenRequestResponse
			logger.info((tokenRequestResponseNodes.item(0).getChildNodes().item(0).getNodeName() + " --- "
					+ tokenRequestResponseNodes.item(0).getChildNodes().item(0).getTextContent()));
			JsonArray jsonArrayRequestResponse = new JsonParser()
					.parse(tokenRequestResponseNodes.item(0).getChildNodes().item(0).getTextContent()).getAsJsonArray();
			JsonObject resultElRequestResponse = jsonArrayRequestResponse.get(0).getAsJsonObject();
			Gson gson = new Gson();
			Map<String, Object> mapRequestResponse = gson.fromJson(resultElRequestResponse, Map.class);

			if ((boolean) mapRequestResponse.get("Success")) {
				// success
				logger.info("Get NWD TOKEN SUCCESSFULLY");
				String NWDTokenStr = (String) mapRequestResponse.get("Token");
				this.NWDToken.set(NWDTokenStr);
				logger.info("Token: " + NWDToken.get());
				if ((String) mapRequestResponse.get("Password") != null
						&& !"".equals((String) mapRequestResponse.get("Password"))) {

					String OTPwdStr = (String) mapRequestResponse.get("Password");
					this.OTPwd.set(OTPwdStr);
					logger.info("One Time Password1: " + OTPwd.get());

				} else {
					// ---request verify and get one-time-password
					SOAPMessage nwdSOAPVerifyRequest = createSOAPRequest(nwd_SOAPAction_url_verify, nwd_nameSpace_url,
							nwd_nameSpace, "", nwd_SOAPAction_verify);
					SOAPMessage nwdSOAPVerifyResponse = soapConnection.call(nwdSOAPVerifyRequest, nwd_url);
					// Print the SOAP Response
					/*
					 * System.out.println("Response SOAP Message:");
					 * nwdSOAPVerifyResponse.writeTo(System.out); System.out.println();
					 */

					SOAPBody nwdSOAPVerifyResponseBody = nwdSOAPVerifyResponse.getSOAPBody();
					NodeList tokenVerifyResponseNodes = nwdSOAPVerifyResponseBody.getChildNodes(); // TokenVerifyResponse
					logger.info((tokenVerifyResponseNodes.item(0).getChildNodes().item(0).getNodeName() + " --- "
							+ tokenVerifyResponseNodes.item(0).getChildNodes().item(0).getTextContent()));
					JsonArray jsonArrayVerifyRespond = new JsonParser()
							.parse(tokenVerifyResponseNodes.item(0).getChildNodes().item(0).getTextContent())
							.getAsJsonArray();
					JsonObject resultElVerifyRespond = jsonArrayVerifyRespond.get(0).getAsJsonObject();
					Map<String, Object> mapVerifyRespond = gson.fromJson(resultElVerifyRespond, Map.class);

					if ((boolean) mapVerifyRespond.get("Success")) {
						// one time Password
						String OTPwdStr = (String) mapVerifyRespond.get("Password");
						this.OTPwd.set(OTPwdStr);
						logger.info("One Time Password2: " + OTPwd.get());

					} else {
						// fail
						logger.error((String) mapVerifyRespond.get("Message"));
						writeJsonStringToResponse(response, false, (String) mapVerifyRespond.get("Message"));
						return;
					}
				}

				if (OTPwd.get() != null && !"".equals(OTPwd.get())) {

					
					/*String content = FileCopyUtils .copyToString(new
					InputStreamReader(tsRequestPPSTemplate.getInputStream(),
					StandardCharsets.UTF_8)) .replace("--key--", NWDToken.get()) .replace("--date--",
					new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
					 
					Map<String, String> receiveFileInfoMap = sendSOAPRequestByXML4PPS(content);*/
					 
					// request peoplesoft server with <Key>&<Data Time>

					//SOAP
					
					SOAPMessage ppsSOAPRequest = createPPSSOAPRequest(pps_SOAPAction_url, pps_nameSpace_url,
							pps_nameSpace, pps_request_elements, pps_SOAPAction);
					
					
					SOAPMessage ppsSOAPResponse = soapConnection.call(ppsSOAPRequest, pps_url);
					logger.info("PPS SOAP CALL DONE...");
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					ppsSOAPResponse.writeTo(out);
					
					SAXReader reader = new SAXReader();
					InputStream is = new ByteArrayInputStream(out.toByteArray());
					
					Document doc = reader.read(is);
					Element envelopeE = doc.getRootElement();
					Element bodyE;
					Element resultE;

					bodyE = envelopeE.element("Body");
					resultE = bodyE.element("ReplyInfo").element("AddResponse");
					List<Element> els = resultE.elements();
					Map<String, String> receiveFileInfoMap = new HashMap<String, String>();
					for(Element el : els) {
						logger.info(el.getName() +" --- " +el.getText());
						receiveFileInfoMap.put(el.getName(), el.getText());
					}

					if ("True".equals(receiveFileInfoMap.get("Success"))) {
						receiveFileInfoMap.put("Password", OTPwd.get());

						try {
							File zipParentFolder = ReceiveZipUtil.decoderAndSave(receiveFileInfoMap, destination);

							File encryptCSVFile = ReceiveZipUtil.outputFile(zipParentFolder,outputTargetLocation,cryptoStreamProvider);
							if (encryptCSVFile != null) {

								String path = encryptCSVFile.getAbsolutePath(); //xxxxx.edf
								String dkey = path + " " + new Date().getTime();
								String ekey = cryptoStreamProvider.encryptText(dkey);
								logger.info("ekey: " + ekey);

								writeJsonStringToResponse(response, true, ekey);
								return;
							} else {
								writeJsonStringToResponse(response, false, "Extract File Failed: csvFile Null");
								return;
							}

						} catch (Exception e) {
							String message = e.getMessage();
							if ("exception.recording.unsupportedEncodingException.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"DecoderAndSave File Failed: cause by unsupportedEncodingException");
							} else if ("exception.decoder.content.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"DecoderAndSave File Failed: cause by recording failed");
							} else if ("exception.output.xml.miss".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by xml file missing");
							} else if ("exception.output.xml.redundant".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by redundant xml file");
							} else if ("exception.output.xml.read.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by reading xml file failed");
							} else if ("exception.output.zipfile.extract.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by extracting zip failed");
							}
							return;
						}

					} else {
						writeJsonStringToResponse(response, false, "No Attachment Return. Request Failed.");
						return;
					}
				} else {
					writeJsonStringToResponse(response, false, "No One Time Password. Stop Doing.");
					return;
				}

			} else {
				// fail
				logger.error((String) mapRequestResponse.get("Message"));
				writeJsonStringToResponse(response, false, (String) mapRequestResponse.get("Message"));
				return;
			}

		} catch (

		SOAPException soape) {
			logger.error(soape.getMessage());
			writeJsonStringToResponse(response, false, "SOAPException");
			return;
		}

	}

	

	@RequestMapping("/triggerRequestForData_uat")
	public void triggerRequestForDataUAT(HttpServletRequest request, HttpServletResponse response) throws IOException, DocumentException {
		logger.info("====== Start triggerRequestForData_uat");
		String ip = request.getRemoteAddr();
		if (enableIPLimit) {
			List<String> whiteIPList = new ArrayList<String>(Arrays.asList(whiteListStr.split(",")));
			if (!whiteIPList.contains(ip)) {
				logger.warn("====== IP not in white List. Reject!!!");
				writeJsonStringToResponse(response, false, "IP not in white List. Reject!!!");
				return;
			}
		}

		try {
			//
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			SOAPMessage nwdSOAPTokenRequest = createSOAPRequest(nwd_SOAPAction_url_request, nwd_nameSpace_url,
					nwd_nameSpace, nwd_request_elements, nwd_SOAPAction_request);
			SOAPMessage nwdSOAPTokenResponse = soapConnection.call(nwdSOAPTokenRequest, nwd_url);
			// Print the SOAP Response
			/*
			 * System.out.println("Response SOAP Message:");
			 * nwdSOAPTokenResponse.writeTo(System.out); System.out.println();
			 */

			SOAPBody nwdSOAPTokenResponseBody = nwdSOAPTokenResponse.getSOAPBody();
			NodeList tokenRequestResponseNodes = nwdSOAPTokenResponseBody.getChildNodes(); // TokenRequestResponse
			logger.info((tokenRequestResponseNodes.item(0).getChildNodes().item(0).getNodeName() + " --- "
					+ tokenRequestResponseNodes.item(0).getChildNodes().item(0).getTextContent()));
			JsonArray jsonArrayRequestResponse = new JsonParser()
					.parse(tokenRequestResponseNodes.item(0).getChildNodes().item(0).getTextContent()).getAsJsonArray();
			JsonObject resultElRequestResponse = jsonArrayRequestResponse.get(0).getAsJsonObject();
			Gson gson = new Gson();
			Map<String, Object> mapRequestResponse = gson.fromJson(resultElRequestResponse, Map.class);

			if ((boolean) mapRequestResponse.get("Success")) {
				// success
				logger.info("Get NWD TOKEN SUCCESSFULLY");
				String NWDTokenStr = (String) mapRequestResponse.get("Token");
				this.NWDToken.set(NWDTokenStr);
				logger.info("Token: " + NWDToken.get());
				if ((String) mapRequestResponse.get("Password") != null
						&& !"".equals((String) mapRequestResponse.get("Password"))) {

					String OTPwdStr = (String) mapRequestResponse.get("Password");
					this.OTPwd.set(OTPwdStr);
					logger.info("One Time Password1: " + OTPwd.get());

				} else {
					// ---request verify and get one-time-password
					SOAPMessage nwdSOAPVerifyRequest = createSOAPRequest(nwd_SOAPAction_url_verify, nwd_nameSpace_url,
							nwd_nameSpace, "", nwd_SOAPAction_verify);
					SOAPMessage nwdSOAPVerifyResponse = soapConnection.call(nwdSOAPVerifyRequest, nwd_url);
					// Print the SOAP Response
					/*
					 * System.out.println("Response SOAP Message:");
					 * nwdSOAPVerifyResponse.writeTo(System.out); System.out.println();
					 */

					SOAPBody nwdSOAPVerifyResponseBody = nwdSOAPVerifyResponse.getSOAPBody();
					NodeList tokenVerifyResponseNodes = nwdSOAPVerifyResponseBody.getChildNodes(); // TokenVerifyResponse
					logger.info((tokenVerifyResponseNodes.item(0).getChildNodes().item(0).getNodeName() + " --- "
							+ tokenVerifyResponseNodes.item(0).getChildNodes().item(0).getTextContent()));
					JsonArray jsonArrayVerifyRespond = new JsonParser()
							.parse(tokenVerifyResponseNodes.item(0).getChildNodes().item(0).getTextContent())
							.getAsJsonArray();
					JsonObject resultElVerifyRespond = jsonArrayVerifyRespond.get(0).getAsJsonObject();
					Map<String, Object> mapVerifyRespond = gson.fromJson(resultElVerifyRespond, Map.class);

					if ((boolean) mapVerifyRespond.get("Success")) {
						// one time Password
						String OTPwdStr = (String) mapVerifyRespond.get("Password");
						this.OTPwd.set(OTPwdStr);
						logger.info("One Time Password2: " + OTPwd.get());

					} else {
						// fail
						logger.error((String) mapVerifyRespond.get("Message"));
						writeJsonStringToResponse(response, false, (String) mapVerifyRespond.get("Message"));
						return;
					}
				}

				if (OTPwd.get() != null && !"".equals(OTPwd.get())) {

					//SOAP
					
					SOAPMessage ppsSOAPRequest = createPPSSOAPRequest(pps_uat_SOAPAction_url, pps_uat_nameSpace_url,
							pps_uat_nameSpace, pps_uat_request_elements, pps_uat_SOAPAction);
					SOAPMessage ppsSOAPResponse = soapConnection.call(ppsSOAPRequest, pps_uat_url);
					logger.info("UAT PPS SOAP CALL DONE...");
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					ppsSOAPResponse.writeTo(out);
					
					SAXReader reader = new SAXReader();
					InputStream is = new ByteArrayInputStream(out.toByteArray());
					
					Document doc = reader.read(is);
					Element envelopeE = doc.getRootElement();
					Element bodyE;
					Element resultE;

					bodyE = envelopeE.element("Body");
					resultE = bodyE.element("ReplyInfo").element("AddResponse");
					List<Element> els = resultE.elements();
					Map<String, String> receiveFileInfoMap = new HashMap<String, String>();
					for(Element el : els) {
						logger.info(el.getName() +" --- " +el.getText());
						receiveFileInfoMap.put(el.getName(), el.getText());
					}

					if ("True".equals(receiveFileInfoMap.get("Success"))) {
						receiveFileInfoMap.put("Password", OTPwd.get());

						try {
							File zipParentFolder = ReceiveZipUtil.decoderAndSave(receiveFileInfoMap, destination);

							File encryptCSVFile = ReceiveZipUtil.outputFile(zipParentFolder,outputTargetLocation,cryptoStreamProvider);
							if (encryptCSVFile != null) {

								String path = encryptCSVFile.getAbsolutePath(); //xxxxx.edf
								String dkey = path + " " + new Date().getTime();
								String ekey = cryptoStreamProvider.encryptText(dkey);
								logger.info("ekey: " + ekey);

								writeJsonStringToResponse(response, true, ekey);
								return;
							} else {
								writeJsonStringToResponse(response, false, "Extract File Failed: csvFile Null");
								return;
							}

						} catch (Exception e) {
							String message = e.getMessage();
							if ("exception.recording.unsupportedEncodingException.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"DecoderAndSave File Failed: cause by unsupportedEncodingException");
							} else if ("exception.decoder.content.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"DecoderAndSave File Failed: cause by recording failed");
							} else if ("exception.output.xml.miss".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by xml file missing");
							} else if ("exception.output.xml.redundant".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by redundant xml file");
							} else if ("exception.output.xml.read.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by reading xml file failed");
							} else if ("exception.output.zipfile.extract.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by extracting zip failed");
							}
							return;
						}

					} else {
						writeJsonStringToResponse(response, false, "No Attachment Return. Request Failed.");
						return;
					}
				} else {
					writeJsonStringToResponse(response, false, "No One Time Password. Stop Doing.");
					return;
				}

			} else {
				// fail
				logger.error((String) mapRequestResponse.get("Message"));
				writeJsonStringToResponse(response, false, (String) mapRequestResponse.get("Message"));
				return;
			}

		} catch (

		SOAPException soape) {
			logger.error(soape.getMessage());
			writeJsonStringToResponse(response, false, "SOAPException");
			return;
		}

	}
	

	
	@RequestMapping("/triggerRequestForData")
	public void triggerRequestForDataXML(HttpServletRequest request, HttpServletResponse response) throws IOException, DocumentException {
		logger.info("====== Start triggerRequestForDataXML");
		String ip = request.getRemoteAddr();
		if (enableIPLimit) {
			List<String> whiteIPList = new ArrayList<String>(Arrays.asList(whiteListStr.split(",")));
			if (!whiteIPList.contains(ip)) {
				logger.warn("====== IP not in white List. Reject!!!");
				writeJsonStringToResponse(response, false, "IP not in white List. Reject!!!");
				return;
			}
		}

		try {
			//
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();

			SOAPMessage nwdSOAPTokenRequest = createSOAPRequest(nwd_SOAPAction_url_request, nwd_nameSpace_url,
					nwd_nameSpace, nwd_request_elements, nwd_SOAPAction_request);
			SOAPMessage nwdSOAPTokenResponse = soapConnection.call(nwdSOAPTokenRequest, nwd_url);
			// Print the SOAP Response
			/*
			 * System.out.println("Response SOAP Message:");
			 * nwdSOAPTokenResponse.writeTo(System.out); System.out.println();
			 */

			SOAPBody nwdSOAPTokenResponseBody = nwdSOAPTokenResponse.getSOAPBody();
			NodeList tokenRequestResponseNodes = nwdSOAPTokenResponseBody.getChildNodes(); // TokenRequestResponse
			logger.info((tokenRequestResponseNodes.item(0).getChildNodes().item(0).getNodeName() + " --- "
					+ tokenRequestResponseNodes.item(0).getChildNodes().item(0).getTextContent()));
			JsonArray jsonArrayRequestResponse = new JsonParser()
					.parse(tokenRequestResponseNodes.item(0).getChildNodes().item(0).getTextContent()).getAsJsonArray();
			JsonObject resultElRequestResponse = jsonArrayRequestResponse.get(0).getAsJsonObject();
			Gson gson = new Gson();
			Map<String, Object> mapRequestResponse = gson.fromJson(resultElRequestResponse, Map.class);

			if ((boolean) mapRequestResponse.get("Success")) {
				// success
				logger.info("Get NWD TOKEN SUCCESSFULLY");
				String NWDTokenStr = (String) mapRequestResponse.get("Token");
				this.NWDToken.set(NWDTokenStr);
				logger.info("Token: " + NWDToken.get());
				if ((String) mapRequestResponse.get("Password") != null
						&& !"".equals((String) mapRequestResponse.get("Password"))) {

					String OTPwdStr = (String) mapRequestResponse.get("Password");
					this.OTPwd.set(OTPwdStr);
					logger.info("One Time Password1: " + OTPwd.get());

				} else {
					// ---request verify and get one-time-password
					SOAPMessage nwdSOAPVerifyRequest = createSOAPRequest(nwd_SOAPAction_url_verify, nwd_nameSpace_url,
							nwd_nameSpace, "", nwd_SOAPAction_verify);
					SOAPMessage nwdSOAPVerifyResponse = soapConnection.call(nwdSOAPVerifyRequest, nwd_url);
					// Print the SOAP Response
					/*
					 * System.out.println("Response SOAP Message:");
					 * nwdSOAPVerifyResponse.writeTo(System.out); System.out.println();
					 */

					SOAPBody nwdSOAPVerifyResponseBody = nwdSOAPVerifyResponse.getSOAPBody();
					NodeList tokenVerifyResponseNodes = nwdSOAPVerifyResponseBody.getChildNodes(); // TokenVerifyResponse
					logger.info((tokenVerifyResponseNodes.item(0).getChildNodes().item(0).getNodeName() + " --- "
							+ tokenVerifyResponseNodes.item(0).getChildNodes().item(0).getTextContent()));
					JsonArray jsonArrayVerifyRespond = new JsonParser()
							.parse(tokenVerifyResponseNodes.item(0).getChildNodes().item(0).getTextContent())
							.getAsJsonArray();
					JsonObject resultElVerifyRespond = jsonArrayVerifyRespond.get(0).getAsJsonObject();
					Map<String, Object> mapVerifyRespond = gson.fromJson(resultElVerifyRespond, Map.class);

					if ((boolean) mapVerifyRespond.get("Success")) {
						// one time Password
						String OTPwdStr = (String) mapVerifyRespond.get("Password");
						this.OTPwd.set(OTPwdStr);
						logger.info("One Time Password2: " + OTPwd.get());

					} else {
						// fail
						logger.error((String) mapVerifyRespond.get("Message"));
						writeJsonStringToResponse(response, false, (String) mapVerifyRespond.get("Message"));
						return;
					}
				}

				if (OTPwd.get() != null && !"".equals(OTPwd.get())) {

					//SOAP
					/*
					SOAPMessage ppsSOAPRequest = createPPSSOAPRequest(pps_uat_SOAPAction_url, pps_uat_nameSpace_url,
							pps_uat_nameSpace, pps_uat_request_elements, pps_uat_SOAPAction);
					SOAPMessage ppsSOAPResponse = soapConnection.call(ppsSOAPRequest, pps_uat_url);
					logger.info("UAT PPS SOAP CALL DONE...");
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					ppsSOAPResponse.writeTo(out);
					
					SAXReader reader = new SAXReader();
					InputStream is = new ByteArrayInputStream(out.toByteArray());
					
					Document doc = reader.read(is);
					Element envelopeE = doc.getRootElement();
					Element bodyE;
					Element resultE;

					bodyE = envelopeE.element("Body");
					resultE = bodyE.element("ReplyInfo").element("AddResponse");
					List<Element> els = resultE.elements();
					Map<String, String> receiveFileInfoMap = new HashMap<String, String>();
					for(Element el : els) {
						logger.info(el.getName() +" --- " +el.getText());
						receiveFileInfoMap.put(el.getName(), el.getText());
					}
					*/

					String content = FileCopyUtils.copyToString(
							new InputStreamReader(tsRequestPPSTemplate.getInputStream(),
							StandardCharsets.UTF_8)).replace("--key--", NWDToken.get()).replace("--date--",
							new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
							 
					Map<String, String> receiveFileInfoMap = sendSOAPRequestByXML4PPS(content);
					
					if ("True".equals(receiveFileInfoMap.get("Success"))) {
						receiveFileInfoMap.put("Password", OTPwd.get());

						try {
							File zipParentFolder = ReceiveZipUtil.decoderAndSave(receiveFileInfoMap, destination);

							File encryptCSVFile = ReceiveZipUtil.outputFile(zipParentFolder,outputTargetLocation,cryptoStreamProvider);
							if (encryptCSVFile != null) {

								String path = encryptCSVFile.getAbsolutePath(); //xxxxx.edf
								String dkey = path + " " + new Date().getTime();
								String ekey = cryptoStreamProvider.encryptText(dkey);
								logger.info("ekey: " + ekey);

								writeJsonStringToResponse(response, true, ekey);
								return;
							} else {
								writeJsonStringToResponse(response, false, "Extract File Failed: csvFile Null");
								return;
							}

						} catch (Exception e) {
							String message = e.getMessage();
							if ("exception.recording.unsupportedEncodingException.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"DecoderAndSave File Failed: cause by unsupportedEncodingException");
							} else if ("exception.decoder.content.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"DecoderAndSave File Failed: cause by recording failed");
							} else if ("exception.output.xml.miss".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by xml file missing");
							} else if ("exception.output.xml.redundant".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by redundant xml file");
							} else if ("exception.output.xml.read.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by reading xml file failed");
							} else if ("exception.output.zipfile.extract.failed".equals(message)) {
								writeJsonStringToResponse(response, false,
										"Extract File Failed: cause by extracting zip failed");
							}
							return;
						}

					} else {
						writeJsonStringToResponse(response, false, "No Attachment Return. Request Failed.");
						return;
					}
				} else {
					writeJsonStringToResponse(response, false, "No One Time Password. Stop Doing.");
					return;
				}

			} else {
				// fail
				logger.error((String) mapRequestResponse.get("Message"));
				writeJsonStringToResponse(response, false, (String) mapRequestResponse.get("Message"));
				return;
			}

		} catch (

		SOAPException soape) {
			logger.error(soape.getMessage());
			writeJsonStringToResponse(response, false, "SOAPException");
			return;
		}

	}


	private SOAPMessage createSOAPRequest(String action_url, String nameSpace_url, String nameSpace,
			String request_elements, String soap_action) throws SOAPException, IOException {

		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();

		createSoapEnvelope(soapMessage, nameSpace_url, nameSpace, request_elements, soap_action);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", action_url);

		soapMessage.saveChanges();

		/* Print the request message, just for debugging purposes */
		System.out.println("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println("\n");

		return soapMessage;

	}

	private void createSoapEnvelope(SOAPMessage soapMessage, String nameSpace_url, String nameSpace,
			String request_elements, String soapAction) throws SOAPException {
		SOAPPart soapPart = soapMessage.getSOAPPart();

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(nameSpace, nameSpace_url);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement(soapAction, nameSpace);

		if (request_elements != "") {
			Gson gson = new Gson();
			Map<String, String> map = gson.fromJson(request_elements, new TypeToken<Map<String, String>>() {
			}.getType());

			List<String> dateFormatList = Arrays.asList(DATE_FORMAT);
			for (Map.Entry<String, String> entry : map.entrySet()) {

				if (dateFormatList.contains(entry.getValue())) {

					MyDateFormat format = null;
					for (MyDateFormat m : MyDateFormat.values()) {
						if (m.getName().equals(entry.getValue())) {
							format = m;
						}
					}
					SOAPElement soape = soapBodyElem.addChildElement(entry.getKey(), nameSpace);
					soape.addTextNode(new SimpleDateFormat(format.getFormat()).format(new Date()));
				} else {
					SOAPElement soape = soapBodyElem.addChildElement(entry.getKey(), nameSpace);
					if ("".equals(entry.getValue()))
						soape.addTextNode(NWDToken.get());
					else
						soape.addTextNode(entry.getValue());
				}
			}
		} else {
			SOAPElement soape = soapBodyElem.addChildElement("token", nameSpace);
			soape.addTextNode(NWDToken.get());
		}

	}

	private SOAPMessage createPPSSOAPRequest(String action_url, String nameSpace_url, String nameSpace,
			String request_elements, String soap_action) throws SOAPException, IOException {

		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();

		createPPSSoapEnvelope(soapMessage, nameSpace_url, nameSpace, request_elements, soap_action);

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", action_url);

		soapMessage.saveChanges();

		/* Print the request message, just for debugging purposes */
		System.out.println("Request PPS SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println("\n");

		return soapMessage;

	}

	private void createPPSSoapEnvelope(SOAPMessage soapMessage, String nameSpace_url, String nameSpace,
			String request_elements, String soapAction) throws SOAPException {
		SOAPPart soapPart = soapMessage.getSOAPPart();
		// Create this based on the SOAP request XML, you can use SOAP UI to get the XML
		// as you like
		// String myNamespace = "typ";
		// String myNamespaceURI =
		// "http://localhost:8099/types/downloadESSJobExecutionDetails";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(nameSpace, nameSpace_url);

		// SOAP Body
		SOAPBody soapBody = envelope.getBody();
		SOAPElement soapBodyElem = soapBody.addChildElement(soapAction, nameSpace);

		Gson gson = new Gson();
		Map<String, String> map = gson.fromJson(request_elements, new TypeToken<Map<String, String>>() {
		}.getType());

		List<String> dateFormatList = Arrays.asList(DATE_FORMAT);
		for (Map.Entry<String, String> entry : map.entrySet()) {

			if (dateFormatList.contains(entry.getValue())) {

				MyDateFormat format = null;
				for (MyDateFormat m : MyDateFormat.values()) {
					if (m.getName().equals(entry.getValue())) {
						format = m;
					}
				}
				SOAPElement soape = soapBodyElem.addChildElement(entry.getKey());
				soape.addTextNode(new SimpleDateFormat(format.getFormat()).format(new Date()));
			} else {
				SOAPElement soape = soapBodyElem.addChildElement(entry.getKey());
				if ("".equals(entry.getValue()))
					soape.addTextNode(NWDToken.get());
				else
					soape.addTextNode(entry.getValue());
			}
		}

	}
	
	@Value("${http.Client.Config.ConnectTimeout.Min}")
	private int connectTimeout;
	
	@Value("${http.Client.Config.ConnectionRequestTimeout.Min}")
	private int connectionRequestTimeout;
	
	@Value("${http.Client.Config.SocketTimeout.Min}")
	private int socketTimeout;

	private Map<String, String> sendSOAPRequestByXML4PPS(String content) throws IOException {

		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		CloseableHttpClient closeableHttpClient = httpClientBuilder.build();
		HttpPost httpPost = new HttpPost(pps_url);
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(connectTimeout*60*1000)// 连接主机服务超时时间
				.setConnectionRequestTimeout(connectionRequestTimeout*60*1000)// 请求超时时间
				.setSocketTimeout(socketTimeout*60*1000)// 数据读取超时时间
				.build();
		httpPost.setConfig(requestConfig);

		String retStr = "";
		Map<String, String> result = new HashMap<String, String>();
		try {
			httpPost.setHeader("Content-Type", "text/xml;charset=UTF-8");
			httpPost.setHeader("SOAPAction", pps_SOAPAction_url);
			StringEntity data = new StringEntity(content.toString(), Charset.forName("UTF-8"));
			httpPost.setEntity(data);
			CloseableHttpResponse httpResponse = closeableHttpClient.execute(httpPost);

			HttpEntity httpEntity = httpResponse.getEntity();
			if (httpEntity != null) {
				retStr = EntityUtils.toString(httpEntity, "UTF-8");
				result = XMLReaderUtil.ReadPPSResponseXml(retStr);

			} else {
				logger.warn("====== No PPS Response!!!");
			}

		} catch (Exception e) {
			logger.error(e.toString());
		} finally { // 释放资源 
			closeableHttpClient.close();
		}
		return result;

	}

	private void writeJsonStringToResponse(HttpServletResponse response, boolean status, String message)
			throws IOException {
		JsonObject resultMessage = new JsonObject();
		resultMessage.addProperty("Status", status);
		resultMessage.addProperty("Message", message);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(resultMessage.toString());
		response.getWriter().flush();
		response.getWriter().close();
	}

	private void writeFileToResponse(HttpServletResponse response, boolean status, File file) throws IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/csv");
		response.setHeader("Content-Disposition", "attachment;filename=" + file.getName());
		response.setHeader("Cache-Control", "max-age=1");
		response.setHeader("Pragma", "public");
		InputStream in = new FileInputStream(file);
		OutputStream out = response.getOutputStream();
		int len;
		byte[] buffer = new byte[1024];
		while ((len = in.read(buffer)) > 0) {
			out.write(buffer, 0, len);
		}
	}
	
	@RequestMapping("/test1")
	public void test(HttpServletResponse response) {
		File encryptFile = new File("D:\\test.edf");
		File testFile = new File("D:\\test.txt");
		try {
			FileCopyUtils.copy(new FileInputStream(testFile),cryptoStreamProvider.writeTo(encryptFile));
			writeFileToResponse(response,true,encryptFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
